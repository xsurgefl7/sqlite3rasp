import Pyro4
import serpent

server= Pyro4.Proxy("PYRO:obj_2e249f1a6ab74135aa6774d4e233fb85@localhost:45063")

def download(file_name):
    output_file = open(file_name, "wb")
    output_file.write(serpent.tobytes(server.download(file_name)))
    output_file.close()
    print ("Downloaded file: {}".format(file_name))
    
download('download.jpg')
