import sqlite3
import Pyro4
import os
import smb
from smb.SMBConnection import SMBConnection
from subprocess import Popen
import RPi.GPIO as GPIO
import time
import datetime
from gpiozero import MotionSensor
from threading import Thread
import smtplib
from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

#Pyro4.config.COMMTIMEOUT = 10

os.chdir('/home/pi/security/pictures/') ##changing directory to save the database and pictures

time.sleep(10)

Thread(target=Popen(['python', '-m', 'Pyro4.naming', '-n', '**************'])).start() ##Threading the server

time.sleep(10)

def SendMail(ImgFileName):
    with open(ImgFileName, 'rb') as f:
        img_data = f.read()

    msg = MIMEMultipart()
    msg['Subject'] = 'subject'
    msg['From'] = 'sqlite3rasp@gmail.com'
    msg['To'] = 'sergiozy6693@gmail.com'

    text = MIMEText("Motion Detected!")
    msg.attach(text)
    image = MIMEImage(img_data, name=os.path.basename(ImgFileName))
    msg.attach(image)

    s = smtplib.SMTP('smtp.gmail.com:587')
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('sqlite3rasp@gmail.com', '***********************')
    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()

def convertToBinaryData(filename):
    # Convert digital data to binary format
    with open(filename, 'rb') as file:
        blobData = file.read()
    return blobData

def create_table():
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python_Image.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS Motion(
            id int, photo blob)''')
        sqliteConnection.commit()
        cursor.close()
    except sqlite3.Error as error:
        print("Failed to insert blob data into sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")
# def vacuum():
#     try:
#         sqliteConnection = sqlite3.connect('SQLite_Python_Image.db')
#         cursor = sqliteConnection.cursor()
#         print("Connected to SQLite")
#         cursor.execute("VACUUM Motion")
#         sqliteConnection.commit()
#         cursor.close()
#     except sqlite3.Error as error:
#         print("Failed to insert blob data into sqlite table", error)
#     finally:
#         if sqliteConnection:
#             sqliteConnection.close()
#             print("the sqlite connection is closed")
def limit_table():
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python_Image.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")
        cursor.execute("""
            DELETE FROM Motion WHERE id=1""")
        sqliteConnection.commit()
        cursor.close()
    except sqlite3.Error as error:
        print("Failed to insert blob data into sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")
def insertBLOB(MotId, Motphoto):
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python_Image.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")
        sqlite_insert_blob_query = """INSERT INTO Motion
                                  (id, photo) VALUES (?, ?)"""

        MotPhoto = convertToBinaryData(Motphoto)
        # Convert data into tuple format
        data_tuple = (MotId, MotPhoto)
        cursor.execute(sqlite_insert_blob_query, data_tuple)
        sqliteConnection.commit()
        print("Image and file inserted successfully as a BLOB into a table")
        cursor.close()

    except sqlite3.Error as error:
        print("Failed to insert blob data into sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("the sqlite connection is closed")

def return_value():
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python_Image.db')
        cursor = sqliteConnection.cursor()
        database_value = ("""
            SELECT max(id) FROM Motion""")
        cursor.execute(database_value)
        result = cursor.fetchone()[0]
        cursor.close()
    except sqlite3.Error as error:
        print("Failed to insert blob data into sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
    return result 
def reorder_table(id_num,id_num2):
    try:
        sqliteConnection = sqlite3.connect('SQLite_Python_Image.db')
        cursor = sqliteConnection.cursor()
        cursor.execute("""UPDATE Motion
                            SET id = ?
                            WHERE id = ?""",(id_num,id_num2,))
        sqliteConnection.commit()
        #cursor.execute(database_value)
        cursor.close()
    except sqlite3.Error as error:
        print("Failed to insert blob data into sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
class ToggleEmail():
    def __init__(self, values):
        self.values = values
    def toggleEmail(self):
        if self.values == True:
            self.values = False
        else:
            self.values = True
        return self.values
p = ToggleEmail(True)
create_table()
@Pyro4.expose  ##SERVER
class Server(object):
    @staticmethod
    def download(file_name):
        return open(file_name, "rb").read()
    @staticmethod
    def value():
        return return_value()
    @staticmethod
    def Email(value):
        global p
        p = ToggleEmail(value)
        p = p.toggleEmail()
        return p
    @staticmethod
    def SecEmail():
        global p
        return p
        
Server.download("SQLite_Python_Image.db")

daemon = Pyro4.Daemon("")  ##ENTER IP ADDRESS OF MACHINE
ns = Pyro4.locateNS()
uri = daemon.register(Server)
ns.register("server", uri)
Thread(target=daemon.requestLoop).start()
num=0
pir = MotionSensor(4)
while True:
        pic1 = "download"
        pic = "/home/pi/security/pictures/" + pic1
        if pir.wait_for_motion():  ##try without if statement, put if statement if it fails.
                print("Motion detected!")
                obj1 = Server.SecEmail()
                num+=1
                if (num == 10):
                    num = 9
                converted = "% s" % num
                os.system('libcamera-still -o' + pic + converted + ".jpg --immediate")

                file_obj = open('download' + converted + '.jpg', 'rb')  ##SAVE TO SMB
                connection = SMBConnection('***',
                        '**************',
                        'socket.gethostname()',
                        '*********',
                        '*********',
                        )
                connection.connect('*********')  # The IP of file server
                connection.storeFile('**********8',  # It's the name of shared folder
                    path=time.strftime('I/%Y-%m-%d %I!%M!%S') + '.jpg',
                    file_obj=file_obj)
                connection.close()

                insertBLOB(num, "download" + converted + ".jpg")
                print(obj1)
                if (obj1):
                    SendMail('download' + converted + '.jpg')
                else:
                    print("Mail is off")

                if (int(return_value()) == 9):
                    limit_table()
                    x2 = 1
                    for x in range(2,10): ##check if 9 or 11 works here. 
                        id1 = x
                        id2 = x2
                        reorder_table(id2,id1)
                        x2+=1
                    #vacuum()  ##try limit_table1() or vacuum()
                    os.system('rm -f download1.jpg')
                    x1 = 1
                    for x in range(2,10): 
                        os.system('mv download' + str(x) + '.jpg' ' download' + str(x1) + '.jpg')
                        x1+=1
        pir.wait_for_no_motion()
