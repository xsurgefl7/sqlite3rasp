import Pyro4

@Pyro4.expose
class Server(object):
    @staticmethod
    def download(file_name):
        return open(file_name, "rb").read()

Server.download("download.jpg")

daemon = Pyro4.Daemon()
ns = Pyro4.locateNS()
uri = daemon.register(Server)
ns.register("server", uri)
print(uri)
daemon.requestLoop()
